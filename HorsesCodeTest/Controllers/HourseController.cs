﻿using HorsesCodeTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HorsesCodeTest.Helpers;
using System.Web;

namespace HorsesCodeTest.Controllers
{
    public class HourseController : ApiController
    {
        //Use only as initial values
        Hourse[] Hourses = new Hourse[] 
        { 
            new Hourse { Id = 1, HorseName = "Black	Caviar", TotalAmount=100 }, 
            new Hourse { Id = 2, HorseName = "Green	Moon", TotalAmount=90 }, 
            new Hourse { Id = 3, HorseName = "Shocking", TotalAmount=80 },
            new Hourse { Id = 4, HorseName = "Delta	Blues", TotalAmount=70 },
            new Hourse { Id = 5, HorseName = "Makybe	Diva", TotalAmount=60 },
            new Hourse { Id = 6, HorseName = "Protectionist", TotalAmount=50 },
            new Hourse { Id = 7, HorseName = "Old	Rowley", TotalAmount=40 },
            new Hourse { Id = 8, HorseName = "Skipton", TotalAmount=30 },
        };

        [HttpGet]
        public IEnumerable<HourseDetails> GetAllHourses()
        {
            var session = HttpContext.Current.Session;
            var HourseList = new List<HourseDetails>();
            if (session != null) {
                if (session["HoursesList"] == null)
                    session["HoursesList"] = HourseList = new Helpers.Helpers().GetHourseDetails(Hourses);
                else
                    HourseList = (List<HourseDetails>)session["HoursesList"];
            }
            return HourseList;
        }

        [HttpGet]
        public IEnumerable<HourseDetails> GetAllHourses(string HourseNumber, string Amount)
        {
            var session = HttpContext.Current.Session;
            var HourseList = new List<HourseDetails>();
            if (session != null)
            {
                HourseList = (List<HourseDetails>)session["HoursesList"];
                session["HoursesList"] = HourseList = new Helpers.Helpers().GetHourseDetails(HourseList, Convert.ToInt32( HourseNumber), Convert.ToInt32(Amount));
            }
            return HourseList;
        }

    }
}
