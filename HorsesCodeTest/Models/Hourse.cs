﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HorsesCodeTest.Models
{
    public class Hourse
    {
        public int Id { get; set; }
        public string HorseName{get; set;}
        public int TotalAmount { get; set; }
    }
}