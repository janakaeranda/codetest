﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HorsesCodeTest.Models
{
    public class HourseDetails
    {
        public int HourseNumber { get; set; }
        public string HorseName { get; set; }
        public int TotalAmount { get; set; }
        public decimal Odd { get; set; }

    }
}