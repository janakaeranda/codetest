﻿var uri = 'api/Hourse';

$(document).ready(function () {
    // Send an AJAX request
    $.getJSON(uri)
        .done(function (data) {
            // On success, 'data' contains a list of hourses.
            RenderTable(data)
        });
});

function GetUpdatedHourseList() {

    var hNumber = $('#HourseNumber').val();
    var amount = $('#amount').val();

    $('#HourseNumber').val('');
    $('#amount').val('');

    if (hNumber != '' && amount != '') {
        $.getJSON(uri, {
            HourseNumber: hNumber,
            Amount: amount
        })
            .done(function (data) {
                RenderTable(data)
            })
            .fail(function (jqXHR, textStatus, err) {
                $('#table_div').text('Error: ' + err);
            });
    }
    else {
        alert('Need a Hourse Number and amount !')
    }
}

function RenderTable(data) {
    //Clear table
    $("#table_div").find("tr:gt(0)").remove();

    //Render table
    $.each(data, function (key, item) {
        // Add a list item for the hourse.
        $("#table_div").append("<tr>" +
            "<td>" + item.HourseNumber + "</td>" +
            "<td>" + item.HorseName + "</td>" +
            "<td>" + item.TotalAmount + "</td>" +
            "<td>" + item.Odd + "</td>" +
            +"</tr>");
    });

}