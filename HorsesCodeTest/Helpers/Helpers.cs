﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HorsesCodeTest.Models;

namespace HorsesCodeTest.Helpers
{
    public class Helpers
    {
        /// <summary>
        /// Get the Hourse details from given hourse collection
        /// </summary>
        /// <param name="Hourses"></param>
        /// <returns></returns>
        public List<HourseDetails> GetHourseDetails(Hourse[] Hourses)
        {
            var HourseDetailsList = new List<HourseDetails>();
            int count = 1;
            foreach (var item in Hourses)
            {
                var hourse = new HourseDetails { 
                    HourseNumber = count,
                    HorseName = item.HorseName,
                    TotalAmount = item.TotalAmount,
                    Odd = 0
                    
                };

                HourseDetailsList.Add(hourse);
                count++;
            }

            return CalculateOdds(HourseDetailsList);
        }

        /// <summary>
        /// Modify hourse list based on given values pair
        /// </summary>
        /// <param name="Hourses"></param>
        /// <param name="HourseNumber"></param>
        /// <param name="Amount"></param>
        /// <returns></returns>
        public List<HourseDetails> GetHourseDetails(List<HourseDetails> Hourses , int HourseNumber , int Amount)
        {
            var Hourse = Hourses.Single(x => x.HourseNumber == HourseNumber);

            //var HourseDetailsList = Hourses.Select(x => { x.TotalAmount = Amount; return x; }).Where(x => x.HourseNumber == HourseNumber);
            Hourses.Remove(Hourse);
            Hourse.TotalAmount = Amount;
            Hourses.Add(Hourse);

            return CalculateOdds(Hourses);
        }

        /// <summary>
        /// Calculate the Odds for a give horse Detail list
        /// </summary>
        /// <param name="DetailList"></param>
        /// <returns></returns>
        private List<HourseDetails> CalculateOdds(List<HourseDetails> DetailList)
        {
            int TotalOfAll = DetailList.AsEnumerable().Sum(x => x.TotalAmount);
            var newHorseList = new List<HourseDetails>();
            foreach (var horse in DetailList)
            {
                horse.Odd = (100 - decimal.Round((((decimal)horse.TotalAmount / (decimal)TotalOfAll) * 100), 1, MidpointRounding.AwayFromZero));
                newHorseList.Add(horse);
            }

            var orderedList = newHorseList.AsEnumerable().OrderBy(x => x.Odd).ToList();

            return orderedList;
        }
    }
}